package com.sage_vaillancourt.pebblisp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    final int AppKeyScriptNum = 0;
    final int AppKeyScriptText = 1;
    final UUID appUuid = UUID.fromString("70ec170a-8e1b-11ea-bc55-0242ac130003");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button submitButton = findViewById(R.id.submitButton);
        final TextView codeTextView = findViewById(R.id.codeTextView);

        submitButton.setOnClickListener(v -> {
            PebbleKit.startAppOnPebble(getApplicationContext(), appUuid);
            PebbleDictionary dict = new PebbleDictionary();
            dict.addString(AppKeyScriptText, String.valueOf(codeTextView.getText()));
            PebbleKit.sendDataToPebble(getApplicationContext(), appUuid, dict);
        });

        findViewById(R.id.shareButton).setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, codeTextView.getText());
            sendIntent.setType("text/plain");

            startActivity(Intent.createChooser(sendIntent, null));
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}